""" Converts numbers to words"""

NUMBERS_WORDS1 = {
    '1':'one', '2':'two', '3':'three', '4':'four', '5':'five', '6':'six',
    '7':'seven', '8':'eight', '9':'nine', '10':'ten', '11':'eleven',
    '12':'twelve', '13':'thirteen', '14':'fourteen', '15':'fifteen',
    '16':'sixteen', '17':'seventeen', '18':'eighteen', '19':'ninteen',
    '0':'zero'}

NUMBERS_WORDS2 = {
    '2':'twenty', '3':'thirty', '4':'forty', '5':'fifty', '6':'sixty',
    '7':'seventy', '8':'eighty', '9':'ninety'}
    

def convert_twodigit(input_string):
    """Converts one or two digit numbers"""
    if len(input_string) is 1:
        return_value = NUMBERS_WORDS1[input_string]

    elif len(input_string) is 2 and int(input_string) < 20:
        return_value = NUMBERS_WORDS1[input_string]

    elif len(input_string) is 2 and int(input_string) >= 20 and input_string[-1] is '0':
        return_value = NUMBERS_WORDS2[input_string[0]]

    elif len(input_string) is 2 and int(input_string) >= 20 and input_string[-1] is not '0':
        return_value = NUMBERS_WORDS2[input_string[0]] + NUMBERS_WORDS1[input_string[1]]

    return return_value


def convert_threedigit(input_string):
    """ Converts three digit numbers"""
    if input_string[1] is '0':
        ending_two = convert_twodigit(input_string[-1])

    if input_string[1] is not '0':
        ending_two = convert_twodigit(input_string[1:])

    leading_value = NUMBERS_WORDS1[input_string[-3]]

    return leading_value + ' hundred ' + ending_two


def convert_fourdigit(input_string):
    """ Converts four digit numbers"""
    ending_three = convert_threedigit(input_string[1:])
    return NUMBERS_WORDS1[input_string[0]] + ' thousand ' + ending_three


def convert_fivedigit(input_string):
    """ Converts five digit numbers"""
    ending_three = convert_threedigit(input_string[2:])
    return convert_twodigit(input_string[0:2]) + ' thousand ' + ending_three


def convert_sixdigit(input_string):
    """ Converts six digit numbers"""
    starting_three = convert_threedigit(input_string[0:3])
    ending_three = convert_threedigit(input_string[3:])
    return starting_three + ' thousand ' + ending_three


def convert_number(input_string):
    """Converts any number with length less than seven"""
    if len(input_string) < 3:
        return convert_twodigit(input_string)

    elif len(input_string) is 3:
        return convert_threedigit(input_string)

    elif len(input_string) is 4:
        return convert_fourdigit(input_string)

    elif len(input_string) is 5:
        return convert_fivedigit(input_string)

    elif len(input_string) is 6:
        return convert_sixdigit(input_string)
