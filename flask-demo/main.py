from flask import request
from model import calculate
from flask import Flask
from flask import render_template
from flask import jsonify
app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/calculate')
def calculate_button_clicked():
    number1 = request.args.get('first')
    number2 = request.args.get('second')

    return jsonify({'value': calculate(number1, number2)})


if __name__ == "__main__":
    app.run()

