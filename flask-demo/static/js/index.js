$(document).ready(function() {
    $('#calculate-button').click(calculateButtonClicked);
});


function calculateButtonClicked() {
    $.ajax({
        url: '/calculate',
        type: 'GET',
        data: {
            first: $('#number1').val(),
            second: $('#number2').val()
        },
        error: function(err) {
            alert('Error happened');
        },
        success: function(result) {
            // show the result on the page
            $('#result-label').text(result.value);
        }
    });
}
