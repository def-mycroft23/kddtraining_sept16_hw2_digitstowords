// The ajax function is bound to '#input_integer' textbox.
$(function() {
    $('#input_integer').bind('input', // Bind the textbox input  to the function.
            function() {
                $.ajax({
                    method: 'GET',
                    // '/send/' page runs the function in main.py
                    // which is used to reference the model.py function.
                    url: '/send/',
                    contentType: 'application/json; charset=utf-8',
                    // Updates the dictionary with the input text.
                    data: { input: $('#input_integer').val() },
                    // Returns the output text in the '#result' paragraph.
                    success: function(data) {
                        $('#result').text(data.value);
                    }
                });
    });
});
