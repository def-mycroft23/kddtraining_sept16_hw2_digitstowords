""" Renders the main page and manipulates the model and view."""
from model import convert_number
from flask import Flask, jsonify, render_template, request

application = Flask(__name__)


@application.route('/')
def render_index():
    """ Renders the main page, js file is referenced in the html file."""
    return render_template('index.html')


@application.route('/send/', methods=['GET'])
def return_output():
    """ Communication between the view and the model"""
    return jsonify({'value': convert_number(request.args.get('input'))})


if __name__ == '__main__':
    application.run(port=5000, debug=True)
